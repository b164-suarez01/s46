import {Row,Col,Button} from 'react-bootstrap';


export default function Banner(){
	return(

		<Row>
			<Col classname="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere </p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>		
		)		
}