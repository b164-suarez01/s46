import {useState} from 'react';
import {Card,Button} from'react-bootstrap';

export default function CourseCard({courseProp}){

	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter,setter] = useState(initialGetterValue)
	
	*/
//setcount= asynchrous
	let[count,setCount] = useState(0)
	let[seat,setCount2] = useState(30)

	function enroll() {
		if(seat<=30 && seat>0){
		setCount(count+1)
		setCount2(seat-1)
		console.log('Enrollees'+count)}
		else{
			
			alert("No more slots");
			seat=0;
			count=30
		}
	}

	//console.log(props.courseProp)

	const {name,description,price}=courseProp;


	return (
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Card.Text>Seat: {seat}</Card.Text>
					<Button variant="primary" onClick={enroll}>Enroll</Button>
				</Card.Body>	
			</Card>

		)
}