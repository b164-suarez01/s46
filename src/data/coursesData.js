const coursesData = [
	{
	id: "wdc001",
	name: "PHP-Laravel",
	description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut facere tempora nobis ratione consequuntur consequatur in ullam odit nihil, ipsum neque, voluptatibus suscipit aliquid laboriosam cupiditate excepturi possimus sint? Repudiandae!",
	price:45000,
	onOffer:true
	},
	{
	id: "wdc002",
	name: "Python-Django",
	description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut facere tempora nobis ratione consequuntur consequatur in ullam odit nihil, ipsum neque, voluptatibus suscipit aliquid laboriosam cupiditate excepturi possimus sint? Repudiandae!",
	price:50000,
	onOffer:true
	},
	{
	id: "wdc003",
	name: "Java-Springboot",
	description:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut facere tempora nobis ratione consequuntur consequatur in ullam odit nihil, ipsum neque, voluptatibus suscipit aliquid laboriosam cupiditate excepturi possimus sint? Repudiandae!",
	price:55000,
	onOffer:true
	}
]

export default coursesData;